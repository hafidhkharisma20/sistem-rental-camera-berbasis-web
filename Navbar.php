<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KWH Rental Kamera</title>

    <link rel="stylesheet" href="style.css">
</head>

<body>
    <nav class="menu">
        <div>
            <h1 class="logo">KWH Rental Kamera</h4>
        </div>
        <ul>
            <li><a href="home.php">Home</a></li>
            <li><a href="#">Equipment</a></li>
            <li><a href="#contact">Contact</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="register.php">Sign Up</a></li>
            <li><a href="logout.php">Logout</a></li>
        </ul>
    </nav>

    <section class="product" id="home">
        <h2>CAMERA</h2>
        <div class="card-wrap">
            <?php
            require_once("./koneksi.php");
            $sql = "SELECT * FROM product WHERE jenis = 'cam'";
            $result = $conn->query($sql);

            while ($row = $result->fetch_assoc()) :
            ?>
                <a href="https://wa.me/+6287846698749?text=Halo%20min,%20apakah%20produk%20<?php echo $row['name'] ?>%20masih%20ada?" target="_blank">
                    <div class="card">
                        <div class="card-body">
                            <img src="img/camera/<?php echo $row['gambar'] ?>" alt="" class="card-image">
                            <div class="card-desc">
                                <div>
                                    <h4><?php echo $row['name'] ?></h4>
                                    <p>Rp. <?php echo $row['harga'] ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>

            <?php endwhile ?>
        </div>
    </section>

    <section class="product">
        <h2>Lensa</h2>
        <div class="card-wrap">
            <?php
            require_once("./koneksi.php");
            $sql = "SELECT * FROM product WHERE jenis = 'len'";
            $result = $conn->query($sql);

            while ($row = $result->fetch_assoc()) :
            ?>
                <a href="https://wa.me/+6287846698749?text=Halo%20min,%20apakah%20produk%20<?php echo $row['name'] ?>%20masih%20ada?" target="_blank">
                    <div class="card">
                        <div class="card-body">
                            <img src="img/lens/<?php echo $row['gambar'] ?>" alt="" class="card-image">
                            <div class="card-desc">
                                <div>
                                    <h4><?php echo $row['name'] ?></h4>
                                    <p>Rp. <?php echo $row['harga'] ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            <?php endwhile ?>
        </div>
    </section>

    <footer>
        <section class="foot about" id="about">
            <h2>About</h2>
            <p>KWH Rental Kamera adalah rental kamera ternama di INDONESIA, memanfaatkan kemajuan teknologi.
                KWH Rental Kamera didirikan sejak tahun 2020 dan telah banyak melayani lebih dari 1000 Fotografer & vidiografer dari seluruh Tanah air</p>
            <img src="img/about.jpg" alt="toko" width="480px">
        </section>
        <section class="foot contact" id="contact">
            <h2>Contact Info</h2>
            <div class="wrapper">
                <label for="address">Address</label>
                <p>JL. Parianom D4/NO.02 Perum Sogaten Madiun</p>
                <label for="email">Email</label>
                <p>hafidhkharisma71@gmail.com</p>
                <label for="number">Telp Number</label>
                <p>(069) 12345678</p>
                <label for="phone">Phone</label>
                <p>087846698749</p>
            </div>
        </section>
    </footer>
</body>

</html>